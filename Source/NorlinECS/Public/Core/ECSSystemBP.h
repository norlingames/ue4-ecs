// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Core/ECSSystem.h"
#include "Core/ECSStats.h"
#include "ECSSystemBP.generated.h"

DECLARE_CYCLE_STAT_EXTERN(TEXT("ECSSystem: BP system"), STAT_ECSSystemBPTime, STATGROUP_ECS, NORLINECS_API);

UCLASS(Abstract, BlueprintType, Blueprintable)
class NORLINECS_API UECSSystemBP : public UECSSystem {
    GENERATED_BODY()

public: // UECSSystem methods

    void BeginPlay() override;

protected: // Internal methods

    void EndPlay() override;
    bool CanProcess(int32 Entity) const override;
    void OnRegistered(int32 Entity) override;
    void OnUnregistered(int32 Entity) override;
    void PreProcess() override;
    void PostProcess() override;
    void ProcessEntity(int32 Entity, float DeltaTime) override;

#if STATS
    TStatId GetStatId() const override;
#endif

protected: // BP Handlers

    UFUNCTION(BlueprintNativeEvent, Category = "ECS|Systems", meta=(DisplayName = "BeginPlay"))
    bool ReceiveBeginPlay();

    UFUNCTION(BlueprintNativeEvent, Category = "ECS|Systems", meta=(DisplayName = "EndPlay"))
    bool ReceiveEndPlay();

    UFUNCTION(BlueprintNativeEvent, Category = "ECS|Systems", meta=(DisplayName = "CanProcess"))
    bool ReceiveCanProcess(int32 Entity) const;

    UFUNCTION(BlueprintNativeEvent, Category = "ECS|Systems", meta=(DisplayName = "OnRegistered"))
    bool ReceiveOnRegistered(int32 Entity);

    UFUNCTION(BlueprintNativeEvent, Category = "ECS|Systems", meta=(DisplayName = "OnUnregistered"))
    bool ReceiveOnUnregistered(int32 Entity);

    UFUNCTION(BlueprintNativeEvent, Category = "ECS|Systems", meta=(DisplayName = "PreProcess"))
    bool ReceivePreProcess();

    UFUNCTION(BlueprintNativeEvent, Category = "ECS|Systems", meta=(DisplayName = "PostProcess"))
    bool ReceivePostProcess();

    UFUNCTION(BlueprintNativeEvent, Category = "ECS|Systems", meta=(DisplayName = "ProcessEntity"))
    bool ReceiveProcessEntity(int32 Entity, float DeltaTime);

};
