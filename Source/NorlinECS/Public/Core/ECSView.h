// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Core/ECSEngine.h"
#include "Core/ECSComponentsList.h"

// https://stackoverflow.com/a/44035510/2907156
// template<typename T>
// class FECSListContainer {
// public:
// 	UECSComponentsList* List;
// };

// template<typename T>
class FECSView {

public: // FECSView methods

	FECSView(UECSEngine* ECS)
		: ECS(ECS)
	{}

	template<typename Component>
	void Init() {
		// if (FECSView::List<Component> != nullptr) {
		// 	return;
		// }

		FECSView::List<Component> = ECS->FindList<Component>();
	}

	template<typename Component, typename Second, typename... Others>
	void Init() {
		Init<Component>();
		Init<Second, Others...>();
		bInited = true;
	}

	template<typename Component>
	Component& Get(int32 Entity) {
		return FECSView::List<Component>->Get<Component>(Entity);
	}

	template<typename Component>
	const Component& Get(int32 Entity) const {
		return FECSView::List<Component>->Get<Component>(Entity);
	}

protected: // Internal methods

	// template<typename Component>
	// void UnInit();

	// template<typename Component, typename Second, typename... Others>
	// void UnInit();

protected: // Variables

	bool bInited = false;

	UECSEngine* ECS = nullptr;

	// The cached List pointers are the same for all views
	template<typename T>
	static UECSComponentsList* List;

};

template<typename T>
UECSComponentsList* FECSView::List = nullptr;