// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Core/ECSSystemBP.h"
#include "Core/ECSStats.h"
#include "ECSSystemBPParallel.generated.h"

DECLARE_CYCLE_STAT_EXTERN(TEXT("ECSSystem: Parallel BP system"), STAT_ECSSystemBPParallelTime, STATGROUP_ECS, NORLINECS_API);

UCLASS(Abstract, BlueprintType, Blueprintable)
class NORLINECS_API UECSSystemBPParallel : public UECSSystemBP {
    GENERATED_BODY()

public: // UECSSystem methods

    virtual void Process(float DeltaTime);

#if STATS
    virtual TStatId GetStatId() const;
#endif

};
