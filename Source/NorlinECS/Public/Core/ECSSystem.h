// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Core/ECSStats.h"
#include "Core/ECSView.h"
#include "ECSSystem.generated.h"

class UWorld;

class UECSEngine;

DECLARE_CYCLE_STAT_EXTERN(TEXT("ECSSystem: Undefined system"), STAT_ECSSystemTime, STATGROUP_ECS, NORLINECS_API);

UCLASS(Abstract)
class NORLINECS_API UECSSystem : public UObject {
	GENERATED_BODY()

public: // UObject

	UECSSystem();
	UWorld* GetWorld() const override;

public: // UECSSystem methods

	void SetECS(UECSEngine* InECS);
	bool IsInited() const;
	void ClearECS();

	// Check if the entity can be processed by the system
	// If it can then register the entity with the system
	void Validate(int32 Entity);

	// Must be implemented to define the list of required Components used by this system
	virtual void RequestInit();

	virtual void BeginPlay();

	virtual void PreProcess();
	virtual void Process(float DeltaTime);
	virtual void PostProcess();

protected: // Internal methods

	template<typename... Components>
	void Init() {
		View = new FECSView(ECS);
		View->Init<Components...>();
	}

	bool Register(int32 Entity);
	bool Unregister(int32 Entity);

	template<typename Component>
	Component& Get(int32 Entity) {
		return View->Get<Component>(Entity);
	}

	template<typename Component>
	const Component& Get(int32 Entity) const {
		return View->Get<Component>(Entity);
	}

	virtual bool CanProcess(int32 Entity) const;
	virtual void EndPlay();
	virtual void OnRegistered(int32 Entity);
	virtual void OnUnregistered(int32 Entity);
	virtual void ProcessEntity(int32 Entity, float DeltaTime);

#if STATS
	virtual TStatId GetStatId() const;
#endif

protected: // Variables

	TArray<int32> Entities;
	TMap<int32, bool> EntitiesState;
	
	int32 Count = 0;

	UPROPERTY(BlueprintReadOnly, Category = "ECS")
	UECSEngine* ECS = nullptr;

private: // Very internal methods

	// Must be inited by Init<Components...>()
	FECSView* View = nullptr;

};
