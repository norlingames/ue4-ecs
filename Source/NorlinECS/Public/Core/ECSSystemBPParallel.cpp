// Fill out your copyright notice in the Description page of Project Settings.

#include "Core/ECSSystemBPParallel.h"
#include "Core/ECSEngine.h"
#include "Async/ParallelFor.h"

DEFINE_STAT(STAT_ECSSystemBPParallelTime);
#if STATS
TStatId UECSSystemBPParallel::GetStatId() const {
    return GET_STATID(STAT_ECSSystemBPParallelTime);
}
#endif

void UECSSystemBPParallel::Process(float DeltaTime) {
    check(ECS);

#if STATS
    TStatId SystemStatId = GetStatId();
#endif

    if (Count == 0) {
        return;
    }

    PreProcess();

    ParallelFor(Count, [&](int32 Entity){
#if STATS
        FScopeCycleCounter SystemStats(SystemStatId);
#endif
        ProcessEntity(Entities[Entity], DeltaTime);
    });

    PostProcess();
}
