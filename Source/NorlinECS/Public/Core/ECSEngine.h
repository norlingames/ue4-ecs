// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

// UE4 includes
#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Subsystems/WorldSubsystem.h"

// NorlinECS includes
#include "Core/ECSComponent.h"
#include "Core/ECSComponentsList.h"
#include "Core/ECSLog.h"
#include "Core/ECSStats.h"

#include "ECSEngine.generated.h"

DECLARE_CYCLE_STAT_EXTERN(TEXT("ECSEngine"), STAT_ECSTime, STATGROUP_ECS, NORLINECS_API);

class UECSSystem;

/*
 * ECS System info for the ECS Engine systems list
 */
USTRUCT(BlueprintType)
struct FECSSystemInfo {
	GENERATED_BODY()

public: // Properties

	// Mostly for development/debug - when false, the system will not be used by the Engine
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "ECS")
	bool bEnabled = true;

	// The ECSSystem class, instance of that class will be created when ECSEngine starts to work
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "ECS")
	TSubclassOf<UECSSystem> SystemClass;

};


/** 
* Tick function that calls UECSEngine::Tick
* Implemented in similar way to FActorTickFunction
**/
USTRUCT()
struct FECSTickFunction : public FTickFunction {
	GENERATED_USTRUCT_BODY()

	/**  UECSEngine  that is the target of this tick **/
	UECSEngine* Target = nullptr;

	/** 
		* Abstract function actually execute the tick. 
		* @param DeltaTime - frame time to advance, in seconds
		* @param TickType - kind of tick for this frame
		* @param CurrentThread - thread we are executing on, useful to pass along as new tasks are created
		* @param MyCompletionGraphEvent - completion event for this task. Useful for holding the completetion of this task until certain child tasks are complete.
	**/
	void ExecuteTick(float DeltaTime, ELevelTick TickType, ENamedThreads::Type CurrentThread, const FGraphEventRef& MyCompletionGraphEvent) override;

	/** Abstract function to describe this tick. Used to print messages about illegal cycles in the dependency graph **/
	FString DiagnosticMessage() override;
	FName DiagnosticContext(bool bDetailed) override;
};

template<>
struct TStructOpsTypeTraits<FECSTickFunction> : public TStructOpsTypeTraitsBase2<FECSTickFunction> {
	enum {
		WithCopy = false
	};
};

/*
 * ECSEngine - the core of the ECS
 */
UCLASS(Abstract, BlueprintType, Blueprintable)
class NORLINECS_API UECSEngine : public UObject {
	GENERATED_BODY()

public: // UObject

	// Sets default values for this actor's properties
	UECSEngine();

	UWorld* GetWorld() const override;

public: // Methods

	void Initialize();
	void Deinitialize(UWorld* World, bool bSessionEnded, bool bCleanupResources);

public: // ECS Engine methods

	int32 GetMaxEntities() const;

	bool IsValidEntity(int32 Entity) const;

	// Register a new Entity
	UFUNCTION(BlueprintCallable, Category = "ECS")
	UPARAM(DisplayName = "Entity") int32 Register();

	// Remove the Entity from the ECS
	UFUNCTION(BlueprintCallable, Category = "ECS")
	bool Unregister(int32 Entity);

	// Force re-validate Systems for the Entity
	// Useful when turning components on/off
	UFUNCTION(BlueprintCallable, Category = "ECS")
	void UpdateEntity(int32 Entity);

	UPROPERTY(EditAnywhere, Category = "ECS: Debug")
	bool bDebug = false;

public: // Counters API
	// TODO: Temporary solution

	// Returns the counter's index
	UFUNCTION(BlueprintCallable, Category = "ECS")
	int32 RegisterCounter(const FName& id);

	// Returns the counter value if index is valid
	// Otherwise return 0
	UFUNCTION(BlueprintCallable, Category = "ECS")
	int32& GetCounter(const int32& index);

	// Increment the counter at index to value
	UFUNCTION(BlueprintCallable, Category = "ECS")
	void IncrementCounter(const int32& index, int32 value = 1);

	// Decrement the counter at index to value
	UFUNCTION(BlueprintCallable, Category = "ECS")
	void DecrementCounter(const int32& index, int32 value = 1);

public: // ECS Systems API

	/* BEGIN Lists getters */

	template <typename Component>
	UECSComponentsList* FindList() const {
		const uint32 TypeID = Component::StaticStruct()->GetUniqueID();
		return ComponentsMap.FindChecked(TypeID);
	}

	template <typename Component>
	Component& Get(int32 Entity) {
		// TODO: Validate Entity
		check(Entity != INDEX_NONE);
		check(bActive);

		UECSComponentsList* List = FindList<Component>();
		return List->Get<Component>(Entity);
	}


	template <typename Component>
	const Component& Get(int32 Entity) const {
		// TODO: Validate Entity
		check(Entity != INDEX_NONE);
		check(bActive);

		UECSComponentsList* List = FindList<Component>();
		return List->Get<Component>(Entity);
	}

	/* BEGIN Component getters for Blueprints */

	// TODO: move BP getters to Views somehow

	// Find the Component of type OutComponent for Entity and copy it's data to the OutComponent struct
	// Return true if data was successfuly found
	UFUNCTION(BlueprintCallable, Category = "ECS", CustomThunk, meta = (CustomStructureParam = "OutComponent"))
	UPARAM(DisplayName = "Success") bool GetComponent(int32 Entity, int32& OutComponent);

	// Using CustomThunk to accept any struct and use it's type to find the ccorresponding component data
	DECLARE_FUNCTION(execGetComponent);

	UFUNCTION(BlueprintCallable, Category = "ECS", CustomThunk, meta = (CustomStructureParam = "Component"))
	UPARAM(DisplayName = "Success") bool SetComponent(int32 Entity, const int32& Component);
	
	DECLARE_FUNCTION(execSetComponent);

	/* END Component getters */

	const TArray<int32>& GetEntities() const {
		return Entities;
	}

public: // Methods

	// Process the registered ECS systems
	void Tick(float DeltaTime);

protected: // ECS Engine internal methods

	void BeginPlay();
	void EndPlay(UWorld* World);

	// Register required ECS systems to the engine
	void Setup();

	// To register a component need to create a corresponding List object, derived from UECSComponentsList
	void RegisterComponentType(UScriptStruct* ComponentType);

	// Internal implementation for BP component getter GetComponent
	bool GetComponentImpl(int32 Entity, const uint32 TypeID, void* OutComponentPtr) const;

	// Internal implementation for BP component setter SetComponent
	bool SetComponentImpl(int32 Entity, const uint32 TypeID, void* ComponentPtr);
	void ProcessUpdateEntity(int32 Entity);

	int32 AddEntity();

protected: // Settings

	UPROPERTY(EditDefaultsOnly, Category=Tick)
	FECSTickFunction PrimaryECSTick;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "ECS: Setup")
	int32 MaxEntities = 1000;

	// Order is important! Systems will be processed in the order of this array
	UPROPERTY(EditDefaultsOnly, Category = "ECS: Setup")
	TArray<FECSSystemInfo> SystemsList;

	UPROPERTY(EditDefaultsOnly, Category = "ECS: Setup")
	TArray<UScriptStruct*> ComponentTypes;

protected: // Variables

	bool bActive = false;

	UPROPERTY()
	TArray<UECSSystem*> Systems;

	UPROPERTY()
	TMap<uint32, UECSComponentsList*> ComponentsMap;

	UPROPERTY()
	TArray<int32> Counters;

	UPROPERTY()
	TMap<FName, int32> CountersMap;

	TArray<int32> Entities;
	TArray<int32> RemovedEntities;
	TArray<int32> UpdatedEntities;

	int32 DebugMemoryOccupied = 0;

};
