// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Core/ECSSystem.h"
#include "Core/ECSStats.h"
#include "ECSSystemParallel.generated.h"

DECLARE_CYCLE_STAT_EXTERN(TEXT("ECSSystem: Undefined parallel system"), STAT_ECSSystemParallelTime, STATGROUP_ECS, NORLINECS_API);

UCLASS(Abstract)
class NORLINECS_API UECSSystemParallel : public UECSSystem {
    GENERATED_BODY()

public: // UECSSystem methods

    virtual void Process(float DeltaTime);

#if STATS
    virtual TStatId GetStatId() const;
#endif

};
