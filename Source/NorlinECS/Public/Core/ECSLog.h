// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"

DECLARE_LOG_CATEGORY_EXTERN(NorlinECS, Verbose, All);

#define ECS_DEBUG(Format, ...) \
	UE_LOG(NorlinECS, Warning, TEXT("%s: " Format), __FUNCTION__, ##__VA_ARGS__)

#define ECS_LOG(Format, ...) \
	UE_LOG(NorlinECS, Display, TEXT("%s: " Format), __FUNCTION__, ##__VA_ARGS__)

#define ECS_WARN(Format, ...) \
	UE_LOG(NorlinECS, Warning, TEXT("%s: " Format), __FUNCTION__, ##__VA_ARGS__)

#define ECS_ERROR(Format, ...) \
	UE_LOG(NorlinECS, Error, TEXT("%s: " Format), __FUNCTION__, ##__VA_ARGS__)
