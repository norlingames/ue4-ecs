// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Core/ECSComponent.h"
#include "ECSComponentsList.generated.h"

UCLASS()
class NORLINECS_API UECSComponentsList : public UObject {
	GENERATED_BODY()

public:

	// Have to use UObject* in GetComponentType to be able to accept it from blueprints
	UScriptStruct* GetComponentStruct() const {
		return ComponentType;
	}

public: // Interface API

	void Init(int32 EntitiesCount);
	void UnInit();

	UObject* GetComponentType() const;

	// Register a component (of ComponentType) for the Entity
	bool Register(int32 Entity);

	TArray<uint8>& GetRawData(int32 Entity);

	template <typename Component>
	Component& Get(int32 Entity) {
		TArray<uint8>& Data = GetRawData(Entity);
		return *(reinterpret_cast<Component*>(Data.GetData()));
	}

	void Reset(int32 Entity);

	UPROPERTY(EditDefaultsOnly, Category = "ECS|Components")
	UScriptStruct* ComponentType = nullptr;

	int32 GetListSize();

private:

	int32 StructSize = 0;

	//TArray<uint8> DefaultData;
	TArray<TArray<uint8>> List;

	TMap<int32, int32> EntityComponentMap;

};
