// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "ECSComponent.generated.h"

USTRUCT(BlueprintType)
struct NORLINECS_API FECSComponent {
	GENERATED_BODY()
	
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "ECS|Components")
	bool bEnabled = false;
};
