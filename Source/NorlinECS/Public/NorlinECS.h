// Copyright Norlin Games. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "Modules/ModuleManager.h"

class FNorlinECSModule : public IModuleInterface {

public: // IModuleInterface

	void StartupModule() override;
	void ShutdownModule() override;

};
