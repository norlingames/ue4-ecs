// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "ECSEntity.generated.h"

class UECSEngine;

/**
 * Interface for ECS Entity
 */
UINTERFACE(Blueprintable)
class NORLINECS_API UECSEntity : public UInterface {
	GENERATED_BODY()
};

class NORLINECS_API IECSEntity {
	GENERATED_BODY()

public: // C++ wrappers

	static UECSEngine* GetEngine(const UObject* obj);
	static int32 GetEntity(const UObject* obj);

public: // BP Interface

	// Returns the ECS Engine where current entity is registered
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, DisplayName = "GetEngine")
	UECSEngine* GetEngineBP() const;
	virtual UECSEngine* GetEngineBP_Implementation() const;

	// Returns the Entity (INDEX_NONE if entity is not registered)
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, DisplayName = "GetEntity")
	int32 GetEntityBP() const;
	virtual int32 GetEntityBP_Implementation() const;

};
