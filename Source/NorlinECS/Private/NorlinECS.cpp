// Copyright Norlin Games. All Rights Reserved.

#include "NorlinECS.h"

#define LOCTEXT_NAMESPACE "FNorlinECSModule"

void FNorlinECSModule::StartupModule() {}
void FNorlinECSModule::ShutdownModule() {}

#undef LOCTEXT_NAMESPACE

IMPLEMENT_MODULE(FNorlinECSModule, NorlinECS)
