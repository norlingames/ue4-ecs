// Fill out your copyright notice in the Description page of Project Settings.

#include "API/ECSEntity.h"

/* static */ UECSEngine* IECSEntity::GetEngine(const UObject* obj) {
	if (const IECSEntity* nativeInterface = Cast<IECSEntity>(obj)) {
		return nativeInterface->GetEngineBP_Implementation();
	}

	if (obj && obj->Implements<UECSEntity>()) {
		// Execute BP-implemented interface
		return IECSEntity::Execute_GetEngineBP(obj);
	}

	return nullptr;
}

/* static */ int32 IECSEntity::GetEntity(const UObject* obj) {
	if (const IECSEntity* nativeInterface = Cast<IECSEntity>(obj)) {
		return nativeInterface->GetEntityBP_Implementation();
	}

	if (obj && obj->Implements<UECSEntity>()) {
		// Execute BP-implemented interface
		return IECSEntity::Execute_GetEntityBP(obj);
	}

	return INDEX_NONE;
}

UECSEngine* IECSEntity::GetEngineBP_Implementation() const {
	return nullptr;
}

int32 IECSEntity::GetEntityBP_Implementation() const {
	return INDEX_NONE;
}
