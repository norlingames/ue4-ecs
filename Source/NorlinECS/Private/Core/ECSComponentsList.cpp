// Fill out your copyright notice in the Description page of Project Settings.

#include "Core/ECSComponentsList.h"
#include "Core/ECSEngine.h"

void UECSComponentsList::Init(int32 EntitiesCount) {
    StructSize = ComponentType->GetStructureSize();

    List.Reserve(EntitiesCount);
    EntityComponentMap.Reserve(EntitiesCount);

    TArray<uint8> DefaultData;
    DefaultData.AddDefaulted(StructSize);

    List.Init(DefaultData, EntitiesCount);
};

void UECSComponentsList::UnInit() {
    List.Empty();
};

UObject* UECSComponentsList::GetComponentType() const {
    return ComponentType;
};

bool UECSComponentsList::Register(int32 Entity) {
    // TODO: Not Implemented

    // When registering a new entity the List array will be unsorted by entities
    // e.g. if Entity 5 will be registered first, and Entity 1 will be registered second
    // then List array will contains data for 5 by index 0, and data for 1 by index 1
    // Before the change, need to test the current no-holes implementation for data locality - 
    // to see if it make sense to keep order or it does not matter

    return false;
}

TArray<uint8>& UECSComponentsList::GetRawData(int32 Entity) {
    //return DefaultData;
    return List[Entity];
};

void UECSComponentsList::Reset(int32 Entity) {
   List[Entity].Empty(StructSize);
};

int32 UECSComponentsList::GetListSize() {
    const int32 DefaultArraySize = static_cast<int32>(List.GetAllocatedSize());
    return FMath::Max(DefaultArraySize, StructSize * List.Num());
}
