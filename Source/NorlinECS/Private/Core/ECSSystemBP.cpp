// Fill out your copyright notice in the Description page of Project Settings.

#include "Core/ECSSystemBP.h"
#include "Core/ECSEngine.h"

DEFINE_STAT(STAT_ECSSystemBPTime);
#if STATS
TStatId UECSSystemBP::GetStatId() const {
    return GET_STATID(STAT_ECSSystemBPTime);
}
#endif


void UECSSystemBP::BeginPlay() {
    ReceiveBeginPlay();
}

void UECSSystemBP::EndPlay() {
    ReceiveEndPlay();
}

bool UECSSystemBP::CanProcess(int32 Entity) const {
    return ReceiveCanProcess(Entity);
}

void UECSSystemBP::OnRegistered(int32 Entity) {
    ReceiveOnRegistered(Entity);
}

void UECSSystemBP::OnUnregistered(int32 Entity) {
    ReceiveOnUnregistered(Entity);
}

void UECSSystemBP::PreProcess() {
    ReceivePreProcess();
}

void UECSSystemBP::PostProcess() {
    ReceivePostProcess();
}

void UECSSystemBP::ProcessEntity(int32 Entity, float DeltaTime) {
    ReceiveProcessEntity(Entity, DeltaTime);
}

bool UECSSystemBP::ReceiveBeginPlay_Implementation() { return false; }
bool UECSSystemBP::ReceiveCanProcess_Implementation(int32 Entity) const {
    return Super::CanProcess(Entity);
}
bool UECSSystemBP::ReceiveEndPlay_Implementation() { return false; }
bool UECSSystemBP::ReceiveOnRegistered_Implementation(int32 Entity) { return false; }
bool UECSSystemBP::ReceiveOnUnregistered_Implementation(int32 Entity) { return false; }
bool UECSSystemBP::ReceivePreProcess_Implementation() { return false; };
bool UECSSystemBP::ReceivePostProcess_Implementation() { return false; };
bool UECSSystemBP::ReceiveProcessEntity_Implementation(int32 Entity, float DeltaTime) { return false; }
