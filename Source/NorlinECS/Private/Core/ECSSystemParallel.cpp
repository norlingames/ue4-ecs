// Fill out your copyright notice in the Description page of Project Settings.

#include "Core/ECSSystemParallel.h"
#include "Core/ECSEngine.h"
#include "Async/ParallelFor.h"

DEFINE_STAT(STAT_ECSSystemParallelTime);
#if STATS
TStatId UECSSystemParallel::GetStatId() const {
    return GET_STATID(STAT_ECSSystemParallelTime);
}
#endif

void UECSSystemParallel::Process(float DeltaTime) {
    check(ECS);

#if STATS
    TStatId SystemStatId = GetStatId();
#endif

    PreProcess();

    if (Count == 0) {
        return;
    }

    TArray<int32> CachedEntities = Entities;

    //ParallelFor(Count, [=](int32 Entity){
    for (int32 Entity : CachedEntities) {
#if STATS
        FScopeCycleCounter SystemStats(SystemStatId);
#endif
        //ProcessEntity(CachedEntities[Entity], DeltaTime);
        ProcessEntity(Entity, DeltaTime);
    }
    //});

    PostProcess();
}
