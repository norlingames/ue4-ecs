// Fill out your copyright notice in the Description page of Project Settings.

#include "Core/ECSEngine.h"
#include "Core/ECSSystem.h"
#include "Core/ECSComponentsList.h"

#include "Async/ParallelFor.h"

DEFINE_STAT(STAT_ECSTime);

void FECSTickFunction::ExecuteTick(float DeltaTime, ELevelTick TickType, ENamedThreads::Type CurrentThread, const FGraphEventRef& MyCompletionGraphEvent) {
	if (!Target || !IsValid(Target) || Target->IsUnreachable()) {
		return;
	}

	if (TickType == LEVELTICK_ViewportsOnly) {
		return;
	}

	FScopeCycleCounterUObject ActorScope(Target);
	Target->Tick(DeltaTime);
}

FString FECSTickFunction::DiagnosticMessage() {
	return Target->GetFullName() + TEXT("[TickECS]");
}

FName FECSTickFunction::DiagnosticContext(bool bDetailed) {
	if (bDetailed) {
		// Format is "ActorNativeClass/ActorClass"
		FString ContextString = FString::Printf(TEXT("%s/%s"), *GetParentNativeClass(Target->GetClass())->GetName(), *Target->GetClass()->GetName());
		return FName(*ContextString);
	}

	return GetParentNativeClass(Target->GetClass())->GetFName();
}

UECSEngine::UECSEngine() {
	PrimaryECSTick.TickGroup = TG_PrePhysics;
	PrimaryECSTick.bCanEverTick = true;
	PrimaryECSTick.bStartWithTickEnabled = true;
	PrimaryECSTick.SetTickFunctionEnable(false); 

	Systems = TArray<UECSSystem*>();

	ComponentsMap = TMap<uint32, UECSComponentsList*>();

	Entities = TArray<int32>();
	RemovedEntities = TArray<int32>();
}

UWorld* UECSEngine::GetWorld() const {
	// CDO objects do not belong to a world
	if (HasAnyFlags(RF_ClassDefaultObject)) {
		return nullptr;
	}

	UObject* Outer = GetOuter();
	if (!Outer || Outer->HasAnyFlags(RF_BeginDestroyed) || Outer->IsUnreachable()) {
		return nullptr;
	}

	UWorld* World = Outer->GetWorld();
	if (World) {
		return World;
	}

	World = GetTypedOuter<UWorld>();
	if (World) {
		return World;
	}

	ULevel* Level = GetTypedOuter<ULevel>();
	if (Level) {
		return Level->OwningWorld;
	}

	return nullptr;
}

void UECSEngine::Initialize() {
	UWorld* World = GetWorld();
	if (!World || !World->IsGameWorld()) {
		// ECS should only work in game world
		return;
	}

	Setup();

	bActive = true;

	if (Systems.Num() == 0) {
		ECS_WARN("No Systems registered!");
	}

	// Begin play trigger
	World->OnWorldBeginPlay.AddUObject(this, &UECSEngine::BeginPlay);

	// Before actors EndPlay
	FWorldDelegates::OnWorldBeginTearDown.AddUObject(this, &UECSEngine::EndPlay);

	// After actors EndPlay
	FWorldDelegates::OnPostWorldCleanup.AddUObject(this, &UECSEngine::Deinitialize);
}

void UECSEngine::Deinitialize(UWorld* World, bool bSessionEnded, bool bCleanupResources) {
	World->OnWorldBeginPlay.RemoveAll(this);
	FWorldDelegates::OnWorldBeginTearDown.RemoveAll(this);
	FWorldDelegates::OnPostWorldCleanup.RemoveAll(this);

	// Clear all components lists
	for (TPair<uint32, UECSComponentsList*> ListPair : ComponentsMap) {
		ListPair.Value->UnInit();
	}

	ComponentsMap.Empty();

	// Clear all systems
	for (UECSSystem* System : Systems) {
		System->ClearECS();
	}

	bActive = false;

	DebugMemoryOccupied = 0;

	Systems.Empty();
}

int32 UECSEngine::GetMaxEntities() const {
	return MaxEntities;
}

void UECSEngine::Setup() {
	Entities.Reserve(MaxEntities);
	RemovedEntities.Reserve(MaxEntities);

	ComponentsMap.Reserve(ComponentTypes.Num());
	for (auto ComponentType : ComponentTypes) {
		RegisterComponentType(ComponentType);
	}

	ECS_LOG("Memory allocated for components data: %d bytes", DebugMemoryOccupied);

	Systems.Reserve(SystemsList.Num());
	for (const FECSSystemInfo& SystemInfo : SystemsList) {
		if (!SystemInfo.bEnabled) {
			continue;
		}

		auto SystemType = SystemInfo.SystemClass;
		auto System = NewObject<UECSSystem>(this, SystemType);

		System->SetECS(this);

		if (!System->IsInited()) {
			ECS_WARN("System '%s' is not inited! Please implement RequestInit() method in the system", *System->GetClass()->GetName());
			continue;
		}

		Systems.Add(System);
	}
}

void UECSEngine::RegisterComponentType(UScriptStruct* ComponentType) {
	UECSComponentsList* List = NewObject<UECSComponentsList>(this);
	List->ComponentType = ComponentType;

	const uint32 TypeID = ComponentType->GetUniqueID();
	ComponentsMap.Emplace(TypeID, List);

	const FName ComponentName = ComponentType->GetFName();
	ECS_LOG("%s (%d), size: %d", *ComponentName.ToString(), TypeID, ComponentType->GetStructureSize());
	List->Init(MaxEntities);

	DebugMemoryOccupied += List->GetListSize();
}

DEFINE_FUNCTION(UECSEngine::execGetComponent) {
	P_GET_PROPERTY(FIntProperty, Entity);

	Stack.StepCompiledIn<FStructProperty>(NULL);
	void* OutComponentPtr = Stack.MostRecentPropertyAddress;

	P_FINISH;

	P_NATIVE_BEGIN;

	FStructProperty* StructProp = CastField<FStructProperty>(Stack.MostRecentProperty);

	if (!StructProp || !OutComponentPtr) {
		FBlueprintExceptionInfo ExceptionInfo(
			EBlueprintExceptionType::AccessViolation,
			FText::FromString("OutComponent must be a Struct!")
		);

		FBlueprintCoreDelegates::ThrowScriptException(P_THIS, Stack, ExceptionInfo);
		*(bool*)RESULT_PARAM = false;
		return;
	}

	UScriptStruct* OutputType = StructProp->Struct;

	*(bool*)RESULT_PARAM = P_THIS->GetComponentImpl(Entity, OutputType->GetUniqueID(), OutComponentPtr);

	P_NATIVE_END;
}

bool UECSEngine::GetComponentImpl(int32 Entity, const uint32 TypeID, void* OutComponentPtr) const {
	// Check for invalid Entity
	check(Entity >= 0 && Entity < MaxEntities);

	if (!ComponentsMap.Contains(TypeID)) {
		return false;
	}

	UECSComponentsList* List = ComponentsMap.FindRef(TypeID);
	TArray<uint8>& Data = List->GetRawData(Entity);

	const UScriptStruct* ComponentType = List->GetComponentStruct();
	ComponentType->CopyScriptStruct(OutComponentPtr, Data.GetData());

	return true;
}

DEFINE_FUNCTION(UECSEngine::execSetComponent) {
	P_GET_PROPERTY(FIntProperty, Entity);

	Stack.StepCompiledIn<FStructProperty>(NULL);
	void* ComponentPtr = Stack.MostRecentPropertyAddress;

	P_FINISH;

	P_NATIVE_BEGIN;

	FStructProperty* StructProp = CastField<FStructProperty>(Stack.MostRecentProperty);

	if (!StructProp || !ComponentPtr) {
		FBlueprintExceptionInfo ExceptionInfo(
			EBlueprintExceptionType::AccessViolation,
			FText::FromString("Component must be a Struct!")
		);

		FBlueprintCoreDelegates::ThrowScriptException(P_THIS, Stack, ExceptionInfo);
		*(bool*)RESULT_PARAM = false;
		return;
	}

	UScriptStruct* OutputType = StructProp->Struct;
	*(bool*)RESULT_PARAM = P_THIS->SetComponentImpl(Entity, OutputType->GetUniqueID(), ComponentPtr);

	P_NATIVE_END;
}

bool UECSEngine::SetComponentImpl(int32 Entity, const uint32 TypeID, void* ComponentPtr) {
	// Check for invalid Entity
	check(Entity >= 0 && Entity < MaxEntities);

	if (!ComponentsMap.Contains(TypeID)) {
		return false;
	}

	UECSComponentsList* List = ComponentsMap.FindRef(TypeID);
	TArray<uint8>& Data = List->GetRawData(Entity);
	
	const UScriptStruct* ComponentType = List->GetComponentStruct();
	ComponentType->CopyScriptStruct(Data.GetData(), ComponentPtr);

	UpdateEntity(Entity);

	return true;
}

void UECSEngine::BeginPlay() {
	for (UECSSystem* System : Systems) {
		System->BeginPlay();
	}

	if (PrimaryECSTick.bCanEverTick) {
		PrimaryECSTick.Target = this;
		PrimaryECSTick.SetTickFunctionEnable(PrimaryECSTick.bStartWithTickEnabled || PrimaryECSTick.IsTickFunctionEnabled());
		PrimaryECSTick.RegisterTickFunction(GetWorld()->GetCurrentLevel());
	}
}

void UECSEngine::EndPlay(UWorld* World) {
	if (PrimaryECSTick.IsTickFunctionRegistered()) {
		PrimaryECSTick.UnRegisterTickFunction();
	}

	// TODO: (?) Add UECSSystem::EndPlay
}

// Called every frame
void UECSEngine::Tick(float DeltaTime) {
	if (!bActive) {
		return;
	}

	// Trigger entity updates after Set
	for (int32 Entity : UpdatedEntities) {
		ProcessUpdateEntity(Entity);
	}

	UpdatedEntities.Reset(MaxEntities);

	{
		SCOPE_CYCLE_COUNTER(STAT_ECSTime);

		for (UECSSystem* System : Systems) {
			System->Process(DeltaTime);
		}
	}
}

bool UECSEngine::IsValidEntity(int32 Entity) const {
	return Entities.Contains(Entity);
}

int32 UECSEngine::AddEntity() {
	int32 NewEntity = Entities.Num();

	if (RemovedEntities.Num() > 0) {
		// take one of the previously used indexes
		NewEntity = RemovedEntities.Pop(false);
	}

	// TODO: (?) allow to dynamically resize the components lists?
	// TODO: #6: May be solved with Issue#6
	if (NewEntity >= MaxEntities) {
		ECS_ERROR("Can't create new entity '%d'! MaxEntities reached: %d", NewEntity, MaxEntities);
		return INDEX_NONE;
	}

	Entities.Add(NewEntity);

	return NewEntity;
}

int32 UECSEngine::Register() {
	int32 Entity = AddEntity();

	// Queue the entity update to let systems know it was added
	UpdateEntity(Entity);

	return Entity;
}

bool UECSEngine::Unregister(int32 Entity) {
	if (!IsValidEntity(Entity)) {
		return false;
	}

	// Clear / disable all the corresponding components
	for (TPair<uint32, UECSComponentsList*> ListPair : ComponentsMap) {
		ListPair.Value->Reset(Entity);
	}

	Entities.RemoveSwap(Entity);
	RemovedEntities.Push(Entity);

	// Queue the entity update to let systems know it was removed
	UpdateEntity(Entity);

	return true;
}

void UECSEngine::UpdateEntity(int32 Entity) {
	UpdatedEntities.Add(Entity);
}

void UECSEngine::ProcessUpdateEntity(int32 Entity) {
	// Automatically register or unregister the given entity for all systems
	for (UECSSystem* System : Systems) {
		System->Validate(Entity);
	}
}

int32 UECSEngine::RegisterCounter(const FName& id) {
	int32* IndexPtr = CountersMap.Find(id);
	if (IndexPtr) {
		return *IndexPtr;
	}

	const int32 NewIndex = Counters.Add(0);
	return CountersMap.Add(id, NewIndex);
}

int32& UECSEngine::GetCounter(const int32& index) {
	ensureMsgf(Counters.IsValidIndex(index), TEXT("UECSEngine::GetCounter: Not valid index '%d'! Use RegisterCounter first"), index);
	return Counters[index];
}

void UECSEngine::IncrementCounter(const int32& index, int32 value) {
	GetCounter(index) += value;
}

void UECSEngine::DecrementCounter(const int32& index, int32 value) {
	GetCounter(index) -= value;
}
