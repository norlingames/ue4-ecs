// Fill out your copyright notice in the Description page of Project Settings.

#include "Core/ECSSystem.h"
#include "Core/ECSEngine.h"
#include "Core/ECSView.h"
#include "Async/ParallelFor.h"

DEFINE_STAT(STAT_ECSSystemTime);
#if STATS
TStatId UECSSystem::GetStatId() const {
	return GET_STATID(STAT_ECSSystemTime);
}
#endif

UECSSystem::UECSSystem() {
	Entities = TArray<int32>();
}

void UECSSystem::SetECS(UECSEngine* InECS) {
	ECS = InECS;

	const int32 MaxEntities = ECS->GetMaxEntities();
	Entities.Reserve(MaxEntities);
	EntitiesState.Reserve(MaxEntities);

	for (int32 Entity = 0; Entity < MaxEntities; ++Entity) {
		EntitiesState.Emplace(Entity, false);
	}

	RequestInit();
}

bool UECSSystem::IsInited() const {
	return View != nullptr;
}

void UECSSystem::ClearECS() {
	EndPlay();

	Entities.Empty();
	EntitiesState.Empty();
	delete View;

	Count = 0;

	ECS = nullptr;
}

UWorld* UECSSystem::GetWorld() const {
	if (HasAllFlags(RF_ClassDefaultObject)) {
		// If we are a CDO, we must return nullptr instead of calling Outer->GetWorld() to fool UObject::ImplementsGetWorld.
		return nullptr;
	}
	
	if (!ECS) {
		return nullptr;
	}

	return ECS->GetWorld();
}

bool UECSSystem::CanProcess(int32 Entity) const {
	return true;
}

void UECSSystem::Validate(int32 Entity) {
	check(ECS);

	bool bRegistered = EntitiesState[Entity];
	bool bCanProcess = ECS->IsValidEntity(Entity) && CanProcess(Entity);

	if (bRegistered == bCanProcess) {
		// Do nothing, it's already in required state
		return;
	}

	if (bCanProcess) {
		Register(Entity);
	} else {
		Unregister(Entity);
	}
}

bool UECSSystem::Register(int32 Entity) {
	if (EntitiesState[Entity]) {
		return false;
	}

	Entities.Add(Entity);
	EntitiesState[Entity] = true;

	Count += 1;

	OnRegistered(Entity);

	return true;
}

bool UECSSystem::Unregister(int32 Entity) {
	if (!EntitiesState[Entity]) {
		return false;
	}

	Entities.Remove(Entity);
	EntitiesState[Entity] = false;

	Count -= 1;

	OnUnregistered(Entity);

	return true;
}

void UECSSystem::Process(float DeltaTime) {
	check(ECS);

#if STATS
	TStatId SystemStatId = GetStatId();
#endif

	PreProcess();

	if (Count == 0) {
		return;
	}

	// Copy entities list since it can be changed during the process
	TArray<int32> CachedEntities = Entities;

	// UE4 Delegates must be updated in the main game thread so no ParallelFor
	for (int32 Entity : CachedEntities) {
#if STATS
		FScopeCycleCounter SystemStats(GetStatId());
#endif
		ProcessEntity(Entity, DeltaTime);
	}

	PostProcess();
}

// These methods are for implementation in subclasses

void UECSSystem::RequestInit() {
	ECS_WARN("Must be overriden in a derived class! Call `Init<Components...>()` to define the component types used by this system");
}
void UECSSystem::BeginPlay() {}
void UECSSystem::EndPlay() {}
void UECSSystem::PreProcess() {}
void UECSSystem::PostProcess() {}
void UECSSystem::OnRegistered(int32 Entity) {}
void UECSSystem::OnUnregistered(int32 Entity) {}
void UECSSystem::ProcessEntity(int32 Entity, float DeltaTime) {}
