// Copyright Norlin Games. All Rights Reserved.

using UnrealBuildTool;

public class NorlinECSEditor : ModuleRules {
	public NorlinECSEditor(ReadOnlyTargetRules Target) : base(Target) {
		PCHUsage = ModuleRules.PCHUsageMode.UseExplicitOrSharedPCHs;
		
		PublicIncludePaths.AddRange(
			new string[] {}
		);
		
		PrivateIncludePaths.AddRange(
			new string[] {
				"NorlinECSEditor/Private",
			}
		);
		
		PublicDependencyModuleNames.AddRange(
			new string[] {
				"Core",
				"NorlinECS",
			}
		);
		
		PrivateDependencyModuleNames.AddRange(
			new string[] {
				"Blutility",
				"CoreUObject",
				"Engine",
				"Slate",
				"SlateCore",
				"UMG",
				"UMGEditor",
				"UnrealEd",
			}
		);
		
		
		DynamicallyLoadedModuleNames.AddRange(
			new string[] {}
		);
	}
}
