// Fill out your copyright notice in the Description page of Project Settings.

#include "Tools/ECSEntityWidget.h"
#include "Components/TextBlock.h"
#include "Engine/Selection.h"
#include "EngineUtils.h"

void UECSEntityWidget::NativeConstruct() {
    Super::NativeConstruct();

    UpdateData();

    // Handle selection change to update Entity data
    USelection::SelectionChangedEvent.AddUObject(this, &UECSEntityWidget::OnSelectionChanged);
}

void UECSEntityWidget::NativeDestruct() {
    USelection::SelectionChangedEvent.RemoveAll(this);

    Super::NativeDestruct();
}

void UECSEntityWidget::NativeTick(const FGeometry& MyGeometry, float InDeltaTime) {
    Super::NativeTick(MyGeometry, InDeltaTime);

    if (Entity == INDEX_NONE) {
        return;
    }

    // Update data in tick to reflect ECS changes
    UpdateData();
}

void UECSEntityWidget::ResetError() {
    if (!TextBlock_Error) {
        return;
    }

    TextBlock_Error->SetVisibility(ESlateVisibility::Collapsed);
    Widget_Content->SetVisibility(ESlateVisibility::Visible);
}

void UECSEntityWidget::ShowError(FText error) {
    if (!TextBlock_Error) {
        return;
    }

    TextBlock_Error->SetText(error);

    TextBlock_Error->SetVisibility(ESlateVisibility::Visible);
    Widget_Content->SetVisibility(ESlateVisibility::Collapsed);
}

void UECSEntityWidget::OnSelectionChanged(UObject* Object) {
    USelection* selection = Cast<USelection>(Object);
    if (!selection) {
        // Nothing is selected
        return;
    }

    // Get first selected actor
    UObject* firstSelectedObject = selection->GetTop(AActor::StaticClass());
    AActor* NewActor = Cast<AActor>(firstSelectedObject);

    // Don't clear data if a wrong actor selected
    if (Actor == NewActor || !NewActor) {
        return;
    }

    Actor = NewActor;
    UpdateData();
}

void UECSEntityWidget::UpdateData() {
    if (!Actor || !GEditor) {
        ShowError(FText::FromString(TEXT("No Entity selected")));
        OnUpdateData(false);
        return;
    }

    ResetError();



    OnUpdateData(true);
}
