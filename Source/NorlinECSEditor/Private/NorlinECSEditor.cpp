// Copyright 2014-2019 Vladimir Alyamkin. All Rights Reserved.

#include "NorlinECSEditor.h"
#include "Modules/ModuleManager.h"

#define LOCTEXT_NAMESPACE "FNorlinECSEditorModule"

void FNorlinECSEditorModule::StartupModule() {}
void FNorlinECSEditorModule::ShutdownModule() {}

#undef LOCTEXT_NAMESPACE

IMPLEMENT_MODULE(FNorlinECSEditorModule, NorlinECSEditor)
