// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "EditorUtilityWidget.h"
#include "ECSEntityWidget.generated.h"

class UTextBlock;

UCLASS()
class NORLINECSEDITOR_API UECSEntityWidget : public UEditorUtilityWidget {
    GENERATED_BODY()

public: // UEditorUtilityWidget

    void NativeConstruct() override;
    void NativeDestruct() override;
    void NativeTick(const FGeometry& MyGeometry, float InDeltaTime) override;

public: // Methods

    UFUNCTION(BlueprintCallable, Category = "ECS|Debug")
    void UpdateData();

    UFUNCTION(BlueprintImplementableEvent, Category = "ECS|Debug")
    void OnUpdateData(bool hasData);

protected: // Internal methods

    void OnSelectionChanged(UObject* Object);

    void ResetError();
    void ShowError(FText error);

protected: // Variables

    UPROPERTY(BlueprintReadOnly, Category = "ECS|Debug")
    AActor* Actor = nullptr;

    UPROPERTY(BlueprintReadWrite, Category = "ECS|Debug")
    int32 Entity = INDEX_NONE;

protected: // UI Elements

    UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "ECS|Debug", meta = (BindWidget))
    UTextBlock* TextBlock_Error = nullptr;

    UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "ECS|Debug", meta = (BindWidget))
    UWidget* Widget_Content = nullptr;

};
